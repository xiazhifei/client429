// client429.h : main header file for the CLIENT429 application
//

#if !defined(AFX_CLIENT429_H__B248487E_9539_4463_9CA1_6683A56B72FC__INCLUDED_)
#define AFX_CLIENT429_H__B248487E_9539_4463_9CA1_6683A56B72FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CClient429App:
// See client429.cpp for the implementation of this class
//

class CClient429App : public CWinApp
{
public:
	CClient429App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClient429App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CClient429App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENT429_H__B248487E_9539_4463_9CA1_6683A56B72FC__INCLUDED_)
