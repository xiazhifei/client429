// client429Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "client429.h"
#include "client429Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClient429Dlg dialog

CClient429Dlg::CClient429Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClient429Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CClient429Dlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClient429Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClient429Dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CClient429Dlg, CDialog)
	//{{AFX_MSG_MAP(CClient429Dlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_READ, OnBtnRead)
	ON_BN_CLICKED(IDC_BTN_WRITE, OnBtnWrite)
	ON_BN_CLICKED(IDC_CK_OPEN, OnCkOpen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClient429Dlg message handlers

BOOL CClient429Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClient429Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClient429Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CClient429Dlg::OnOK() 
{
	// TODO: Add extra validation here	
	//CDialog::OnOK();
}

HANDLE hpipe = NULL;

void CClient429Dlg::OnBtnRead() 
{
	char buf[1024];
	ZeroMemory(buf,sizeof(buf));

	DWORD  dwRead = NULL; 
	if (PeekNamedPipe(hpipe,buf,128,&dwRead,0,0))
		if(dwRead)
			ReadFile(hpipe, buf, dwRead, &dwRead, NULL);
	
	if(dwRead)
		SetDlgItemText(IDC_ED_RX, buf);	
	else
		SetDlgItemText(IDC_ED_RX, "");	
}

void CClient429Dlg::OnBtnWrite() 
{
	CString str;
	GetDlgItemText(IDC_ED_TX, str);	

	DWORD dwWrite = NULL;    
	WriteFile(hpipe, (LPSTR)(LPCSTR)str, str.GetLength(), &dwWrite, NULL);  	
}



void CClient429Dlg::OnCkOpen() 
{	
	CString str;
	char buf[128];
	GetDlgItemText(IDC_ED_NAME, str);	
	sprintf(buf,"\\\\.\\pipe\\%s",str);	
	if( ((CButton*)GetDlgItem(IDC_CK_OPEN))->GetCheck())
	{
		hpipe = CreateFile(buf,									// pointer to name of the file
			GENERIC_READ|GENERIC_WRITE,							// access (read-write) mode
			0,													// share mode
			NULL,												// pointer to security attributes
			OPEN_EXISTING,										// how to create
			FILE_FLAG_OVERLAPPED,								// file attributes
			NULL);												// handle to file with attributes to copy
		if(INVALID_HANDLE_VALUE != hpipe)
		{
			GetDlgItem(IDC_ED_NAME)->EnableWindow(FALSE);
			((CButton*)GetDlgItem(IDC_CK_OPEN))->SetCheck(1);
		}
		else
		{
			((CButton*)GetDlgItem(IDC_CK_OPEN))->SetCheck(0);
		}
	}	
	else
	{
		CloseHandle(hpipe);
		GetDlgItem(IDC_ED_NAME)->EnableWindow();
	}	
}
