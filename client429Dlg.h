// client429Dlg.h : header file
//

#if !defined(AFX_CLIENT429DLG_H__3CA49252_6DA6_42AE_9F80_D7B157086466__INCLUDED_)
#define AFX_CLIENT429DLG_H__3CA49252_6DA6_42AE_9F80_D7B157086466__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CClient429Dlg dialog

class CClient429Dlg : public CDialog
{
// Construction
public:
	CClient429Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CClient429Dlg)
	enum { IDD = IDD_CLIENT429_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClient429Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CClient429Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnBtnRead();
	afx_msg void OnBtnWrite();
	afx_msg void OnCkOpen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENT429DLG_H__3CA49252_6DA6_42AE_9F80_D7B157086466__INCLUDED_)
